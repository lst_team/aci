/*
 * This file is part of the Sonatra package.
 *
 * (c) François Pluchino <francois.pluchino@sonatra.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*global define*/
/*global jQuery*/
/*global window*/
/*global navigator*/
/*global document*/
/*global CSSMatrix*/
/*global WebKitCSSMatrix*/
/*global MSCSSMatrix*/
/*global Hammer*/
/*global HammerScroll*/

/**
 * @param {jQuery} $
 *
 * @typedef {object}           define.amd                     The requirejs AMD
 * @typedef {HammerScroll}     HammerScroll                   The hammer scroll instance
 * @typedef {Number|undefined} HammerScroll.dragStartPosition The drag start position
 * @typedef {Number|undefined} HammerScroll.dragDirection     The hammer direction on drag start
 * @typedef {jQuery|undefined} HammerScroll.$scrollbar        The jquery scrollbar
 */
//(function($) {
  
  (function (factory) {
    'use strict';

    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    'use strict';

    /**
     * Check if is a mobile device.
     *
     * @return {boolean}
     *
     * @private
     */
    function mobileCheck() {
        return Boolean(navigator.userAgent.match(/Android|iPhone|iPad|iPod|IEMobile|BlackBerry|Opera Mini/i));
    }

    /**
     * Check is browser is Firefox.
     *
     * @return Boolean
     */
    function isFirefox() {
        return -1 !== navigator.userAgent.toLowerCase().indexOf('firefox');
    }

    /**
     * Changes the css transition configuration on target element.
     *
     * @param {HammerScroll} self         The hammer scroll instance
     * @param {jQuery}       $target      The element to edited
     * @param {string}       [transition] The css transition configuration of target
     *
     * @private
     */
    function changeTransition(self, $target, transition) {
        if (undefined === transition || null === transition) {
            transition = 'transform ' + self.options.inertiaDuration + 's';

            if (null !== self.options.inertiaFunction) {
                transition += ' ' + self.options.inertiaFunction;
            }
        }

        if ('' === transition) {
            $target.css('-webkit-transition', transition);
            $target.css('transition', transition);
        }

        $target.get(0).style['-webkit-transition'] = 'none' === transition ? transition : '-webkit-' + transition;
        $target.get(0).style.transition = transition;
    }

    /**
     * Changes the css transform configuration on target element.
     *
     * @param {jQuery} $target   The element to edited
     * @param {string} transform The css transform configuration of target
     *
     * @private
     */
    function changeTransform($target, transform) {
        $target.css('-webkit-transform', transform);
        $target.css('transform', transform);
    }

    /**
     * Translate the jquery element with Translate 3D CSS.
     *
     * @param {jQuery } $target  The jquery element
     * @param {Boolean} vertical Check if the vertical direction
     * @param {Number}  delta    The delta of translate
     */
    function changeTranslate($target, vertical, delta) {
        var trans = vertical ?
                '0px, ' + delta + 'px, 0px'
            : delta + 'px, 0px, 0px';

        changeTransform($target, 'translate3d(' + trans + ')');
    }

    /**
     * Get the real outer size of content.
     *
     * @param {jQuery}  $content The jquery content
     * @param {Boolean} vertical Check if the vertical direction
     *
     * @return {Number}
     */
    function getRealOuterSize($content, vertical) {
        var $children = $content.children(),
            params = vertical ?
                    ['top', 'bottom']
                : ['left', 'right'],
            size = vertical ?
                    Math.max($content.outerHeight(), $content.prop('scrollHeight'))
                : Math.max($content.outerWidth(), $content.prop('scrollWidth'));

        return size +
            parseInt($children.first().css('margin-' + params[0]), 10) +
            parseInt($children.last().css('margin-' + params[1]), 10);
    }

    /**
     * Get the real top or left position of content.
     *
     * @param {jQuery}  $content The jquery content
     * @param {Boolean} vertical Check if the vertical direction
     *
     * @return {Number}
     */
    function getRealPosition($content, vertical) {
        var $parent = $content.parent(),
            position = $content.position(),
            param = vertical ? 'top' : 'left';

        return position[param] -
            parseInt($content.children().first().css('margin-' + param), 10) -
            parseInt($parent.css('margin-' + param), 10) -
            parseInt($parent.css('padding-' + param), 10);
    }

    /**
     * Get the vertical/horizontal position of target element.
     *
     * @param {jQuery}  $target  The jquery target
     * @param {Boolean} vertical Check if the vertical direction
     *
     * @return {number}
     *
     * @private
     */
    function getPosition($target, vertical) {
        var transformCss = $target.css('transform'),
            transform = {e: 0, f: 0},
            reMatrix,
            match;

        if (transformCss) {
            if ('function' === typeof CSSMatrix) {
                transform = new CSSMatrix(transformCss);

            } else if ('function' === typeof WebKitCSSMatrix) {
                transform = new WebKitCSSMatrix(transformCss);

            } else if ('function' === typeof MSCSSMatrix) {
                transform = new MSCSSMatrix(transformCss);

            } else {
                reMatrix = /matrix\(\s*-?\d+(?:\.\d+)?\s*,\s*-?\d+(?:\.\d+)?\s*,\s*-?\d+(?:\.\d+)?\s*,\s*-?\d+(?:\.\d+)?\s*,\s*(-?\d+(?:\.\d+)?)\s*,\s*(-?\d+(?:\.\d+)?)\s*\)/;
                match = transformCss.match(reMatrix);

                if (match) {
                    transform.e = parseInt(match[1], 10);
                    transform.f = parseInt(match[2], 10);
                }
            }
        }

        return vertical ? transform.f : transform.e;
    }

    /**
     * Set the vertical/horizontal scroll position of target element.
     *
     * @param {jQuery}  $target  The jquery target
     * @param {Boolean} vertical Check if the vertical direction
     * @param {Number}  delta    The delta
     *
     * @return {Number}
     */
    function setScrollPosition($target, vertical, delta) {
        return vertical ?
                $target.scrollTop(delta)
            : $target.scrollLeft(delta);
    }

    /**
     * Get the vertical/horizontal scroll position of target element.
     *
     * @param {jQuery}  $target  The jquery target
     * @param {Boolean} vertical Check if the vertical direction
     *
     * @return {Number}
     */
    function getScrollPosition($target, vertical) {
        return vertical ?
                $target.scrollTop()
            : $target.scrollLeft();
    }

    /**
     * Trigger the event.
     *
     * @param {String} name The event name
     *
     * @param {HammerScroll} self The hammer scroll instance
     *
     * @private
     */
    function trigger(name, self) {
        var event = $.Event(name + '.st.hammerscroll');
        event.hammerScroll = self;

        self.$element.trigger(event);
    }

    /**
     * Refreshs the scrollbar position.
     *
     * @param {HammerScroll}        self           The hammer scroll instance
     * @param {boolean}             usedTransition Used the transition
     * @param {number}              position       The new position of content
     *
     * @private
     */
    function refreshScrollbarPosition(self, usedTransition, position) {
        if (undefined === self.$scrollbar) {
            return;
        }

        var useScroll = self.options.useScroll,
            wrapperSize = self.isVertical ?
                    self.$element.innerHeight()
                : self.$element.innerWidth(),
            contentSize = useScroll ?
                    (self.isVertical ? self.$content.get(0).scrollHeight : self.$content.get(0).scrollWidth)
                : getRealOuterSize(self.$content, self.isVertical),
            percentScroll = (useScroll ? position : -position) / (contentSize - wrapperSize),
            scrollbarSize = self.isVertical ?
                    self.$scrollbar.outerHeight()
                : self.$scrollbar.outerWidth(),
            delta = Math.round(percentScroll * (wrapperSize - scrollbarSize));

        changeTransition(self, self.$scrollbar, usedTransition ? null : 'none');
        changeTranslate(self.$scrollbar, self.isVertical, delta);

        trigger('refreshscrollbar', self);
    }

    /**
     * Refresh the sticky header on end of scroll inertia transition.
     *
     * @param {jQuery.Event|Event|object} event
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @private
     */
    function dragTransitionEnd(event) {
        var self = event.data,
            position = self.options.useScroll ?
                    getScrollPosition(self.$content, self.isVertical)
                : getRealPosition(self.$content, self.isVertical);

        self.$content.off('transitionend msTransitionEnd oTransitionEnd', dragTransitionEnd);
        refreshScrollbarPosition(self, true, position);

        if (undefined !== self.stickyHeader) {
            self.stickyHeader.checkPosition();
        }

        trigger('scrolling', self);
    }

    /**
     * Limits the vertical/horizontal position with top/left or bottom/right
     * wrapper position (with or without the max bounce).
     *
     * @param {HammerScroll} self        The hammer scroll instance
     * @param {Number}       delta
     * @param {Number}       maxBounce
     * @param {Boolean}      [inertia]
     * @param {Number}       [velocity]
     *
     * @return {Number} The limited position value
     *
     * @private
     */
    function limitPositionValue(self, delta, maxBounce, inertia, velocity) {
        var useScroll = self.options.useScroll,
            wrapperSize,
            size,
            maxScroll,
            position,
            inertiaVal;

        if (undefined === self.dragStartPosition) {
            self.dragStartPosition = useScroll ?
                    -getScrollPosition(self.$content, self.isVertical)
                : getPosition(self.$content, self.isVertical);
        }

        wrapperSize = self.isVertical ?
                self.$element.innerHeight()
            : self.$element.innerWidth();
        size = useScroll ?
                (self.isVertical ? self.$content.get(0).scrollHeight : self.$content.get(0).scrollWidth)
            : getRealOuterSize(self.$content, self.isVertical);
        maxScroll = size - wrapperSize + maxBounce;
        position = -Math.round(delta + self.dragStartPosition);

        // inertia
        if (inertia) {
            inertiaVal = -delta * Math.abs(velocity) * (1 + self.options.inertiaVelocity);
            position = Math.round(position + inertiaVal);
        }

        // start bounce
        if (position < -maxBounce) {
            position = -maxBounce;

        // end bounce with scroll
        } else if (size > wrapperSize) {
            if (position > maxScroll) {
                position = maxScroll;
            }

        // end bounce without scroll
        } else {
            if (0 === maxBounce) {
                position = 0;

            } else if (position > maxBounce) {
                position = maxBounce;
            }
        }

        return position;
    }

    /**
     * Get the width of native scrollbar.
     *
     * @returns {Number}
     */
    function getNativeScrollWidth() {
        var sbDiv = document.createElement("div"),
            size;
        sbDiv.style.width = '100px';
        sbDiv.style.height = '100px';
        sbDiv.style.overflow = 'scroll';
        sbDiv.style.position = 'absolute';
        sbDiv.style.top = '-9999px';

        document.body.appendChild(sbDiv);
        size = sbDiv.offsetWidth - sbDiv.clientWidth;
        document.body.removeChild(sbDiv);

        return size;
    }

    /**
     * Wraps the content.
     *
     * @param {HammerScroll} self The hammer scroll instance
     *
     * @return {jQuery} The content
     *
     * @private
     */
    function wrapContent(self) {
        var scrollSize = getNativeScrollWidth(),
            contentCss = {},
            $content = $('<div class="' + self.options.contentWrapperClass + '"></div>'),
            scrollType = mobileCheck() ? 'auto' : 'scroll';

        if (null !== self.options.contentSelector) {
            $content = $(self.options.contentSelector, self.$element);
            $content.addClass(self.options.contentWrapperClass);
        }

        if (self.options.useScroll) {
            contentCss.position = 'relative';
            contentCss.display = 'block';
            contentCss.overflow = 'hidden';

            if (self.isVertical) {
                contentCss.width = 'auto';
                contentCss.height = '100%';

                if (self.options.nativeScroll) {
                    contentCss['overflow-x'] = 'auto';
                    contentCss['overflow-y'] = scrollType;
                    contentCss['margin-right'] = -scrollSize + 'px';
                }

            } else {
                contentCss.width = '100%';
                contentCss.height = 'auto';

                if (self.options.nativeScroll) {
                    contentCss['overflow-x'] = scrollType;
                    contentCss['overflow-y'] = 'auto';
                    contentCss['margin-bottom'] = -scrollSize + 'px';
                }
            }

            $content.css(contentCss);
        }

        self.$element.css({
            'position': 'relative',
            'overflow': 'hidden'
        });

        if (null === self.options.contentSelector) {
            self.$element.children().each(function () {
                $content.append(this);
            });
            self.$element.append($content);
        }

        return $content;
    }

    /**
     * Unwraps the content.
     *
     * @param {HammerScroll} self The hammer scroll instance
     *
     * @return null
     *
     * @private
     */
    function unwrapContent(self) {
        if (undefined !== self.$scrollbar) {
            self.$scrollbar.remove();
        }

        self.$element.css({
            'position': '',
            'overflow': ''
        });

        if (null === self.options.contentSelector) {
            self.$content.remove();
            self.$content.children().each(function () {
                self.$element.append(this);
            });

            return null;
        }

        self.$content
            .removeClass(self.options.contentWrapperClass)
            .css({
                'position': '',
                'display': '',
                'overflow': '',
                'width': '',
                'height': '',
                'margin-right': '',
                'margin-bottom': ''
            });

        return null;
    }

    /**
     * Creates the scrollbar.
     *
     * @param {HammerScroll} self      The hammer scroll instance
     * @param {String}       direction The direction
     *
     * @return {jQuery} The scrollbar
     *
     * @private
     */
    function generateScrollbar(self, direction) {
        var $scrollbar = $('<div class="hammer-scrollbar ' + direction + '"></div>');

        if (self.options.scrollbarInverse) {
            $scrollbar.addClass('hammer-scroll-inverse');
        }

        self.$element.prepend($scrollbar);

        return $scrollbar;
    }

    /**
     * Action on native scrolling.
     *
     * @param {jQuery.Event|Event} event
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @private
     */
    function onScrolling(event) {
        var self = event.data,
            position = getScrollPosition(self.$content, self.isVertical);

        refreshScrollbarPosition(self, false, position);

        if (undefined !== self.stickyHeader) {
            self.stickyHeader.checkPosition();
        }

        trigger('scrolling', self);
    }

    /**
     * Action on mouse scroll event.
     *
     * @param {jQuery.Event|Event} event
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @private
     */
    function onMouseScrollNative(event) {
        var self = event.data,
            delta = (event.originalEvent.type === 'DOMMouseScroll' ?
                    event.originalEvent.detail * -40 :
                    event.originalEvent.wheelDelta),
            position = getScrollPosition(self.$content, self.isVertical),
            maxPosition = self.isVertical ?
                    self.$content.get(0).scrollHeight - self.$content.innerHeight()
                : self.$content.get(0).scrollWidth - self.$content.innerWidth();

        if ((!self.options.nativeScroll && self.isVertical) ||
                (self.options.useScroll && !self.isVertical && event.shiftKey)) {
            delta = position - delta;
            setScrollPosition(self.$content, self.isVertical, delta);
            onScrolling(event);
            event.stopPropagation();
            event.preventDefault();

        } else if (self.isVertical || (!self.isVertical && event.shiftKey)) {
            if ((delta > 0 && position <= 0) || (delta < 0 && position >= maxPosition)) {
                event.stopPropagation();
                event.preventDefault();
            }
        }
    }

    /**
     * Action on mouse scroll event.
     *
     * @param {jQuery.Event|Event} event
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @private
     */
    function onMouseScrollCssTransform(event) {
        var self = event.data,
            position = -getRealPosition(self.$content, self.isVertical),
            wrapperSize = self.isVertical ?
                    self.$element.innerHeight()
                : self.$element.innerWidth(),
            contentSize = getRealOuterSize(self.$content, self.isVertical),
            delta = (event.originalEvent.type === 'DOMMouseScroll' ?
                    event.originalEvent.detail * -40 :
                    event.originalEvent.wheelDelta);

        if (!self.isVertical && !event.shiftKey) {
            return;
        }

        event.stopPropagation();
        event.preventDefault();

        position -= delta;
        position = Math.max(position, 0);

        if (contentSize <= wrapperSize) {
            position = 0;

        } else if ((contentSize - position) < wrapperSize) {
            position = contentSize - wrapperSize;
        }

        changeTransition(self, self.$content, 'none');
        changeTranslate(self.$content, self.isVertical, -position);
        refreshScrollbarPosition(self, false, -position);

        if (undefined !== self.stickyHeader) {
            self.stickyHeader.checkPosition();
        }

        trigger('scrolling', self);
    }

    /**
     * Action on mouse scroll event.
     *
     * @param {jQuery.Event|Event} event
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @private
     */
    function onMouseScroll(event) {
        if (event.data.options.useScroll) {
            onMouseScrollNative(event);

        } else {
            onMouseScrollCssTransform(event);
        }
    }

    /**
     * Validate the options.
     *
     * @param {Object} options
     */
    function validateOptions(options) {
        var autoConf = options.autoConfig;

        if (null !== options.contentSelector) {
            options.useScroll = true;
        }

        if (autoConf && !options.forceNativeScroll && isFirefox() &&
                options.direction === HammerScroll.DIRECTION_VERTICAL) {
            options.forceNativeScroll = true;
        }

        if (autoConf && (options.nativeScroll || options.forceNativeScroll) && isFirefox() &&
                options.direction === HammerScroll.DIRECTION_HORIZONTAL) {
            options.nativeScroll = false;
        }

        if (typeof Hammer !== 'function') {
            options.forceNativeScroll = true;
        }

        if (autoConf  && options.mobileAutoConfig && mobileCheck()) {
            options.forceNativeScroll = true;
            options.scrollbar = false;
        }

        if (options.forceNativeScroll) {
            options.nativeScroll = true;
        }

        if (options.nativeScroll) {
            options.useScroll = true;
            options.eventDelegated = true;
        }

        if (options.direction !== HammerScroll.DIRECTION_VERTICAL) {
            options.hammerStickyHeader = false;
        }
    }

    /**
     * Prevent scroll event (blocks the scroll on the tab keyboard event with
     * the item is outside the wrapper).
     *
     * @param {jQuery.Event|Event} event
     *
     * @private
     */
    function preventScroll(event) {
        setScrollPosition($(event.target), event.data.isVertical, 0);
    }

    /**
     * Prevent click event.
     *
     * @param {jQuery.Event|Event} event
     *
     * @private
     */
    function preventClick(event) {
        event.preventDefault();
        event.data.$content.off('click.st.hammerscroll dragstart.st.hammerscroll drag.st.hammerscroll dragend.st.hammerscroll', 'a', preventClick);
    }

    /**
     *
     * @param {HammerScroll} self The hammer scroll instance
     *
     * @return {Number}
     */
    function getHammerDirection(self) {
        if (self.options.direction === HammerScroll.DIRECTION_VERTICAL) {
            return Hammer.DIRECTION_VERTICAL;
        }

        if (self.options.direction === HammerScroll.DIRECTION_HORIZONTAL) {
            return Hammer.DIRECTION_HORIZONTAL;
        }

        return Hammer.DIRECTION_NONE;
    }

    // HAMMER SCROLL CLASS DEFINITION
    // ==============================

    /**
     * @constructor
     *
     * @param {string|elements|object|jQuery} element
     * @param {object}                        options
     *
     * @typedef {Manager} HammerScroll.hammer The hammer manager
     *
     * @this HammerScroll
     */
    var HammerScroll = function (element, options) {
        this.guid     = jQuery.guid;
        this.options  = $.extend(true, {}, HammerScroll.DEFAULTS, options);
        this.$element = $(element).eq(0);

        validateOptions(this.options);
        this.isVertical = this.options.direction === HammerScroll.DIRECTION_VERTICAL;
        this.$content = wrapContent(this);

        if (this.options.hammerStickyHeader && $.fn.stickyHeader) {
            this.stickyHeader = this.$element.stickyHeader().data('st.stickyheader');
        }

        if (!this.options.useScroll) {
            setScrollPosition(this.$content, this.isVertical, 0);
            $(window).on('resize.st.hammerscroll' + this.guid, null, this, this.resizeScroll);
            this.$content.on('scroll.st.hammerscroll', preventScroll);
        }

        if (this.options.nativeScroll) {
            this.$content.on('scroll.st.hammerscroll', null, this, onScrolling);
        }

        if (this.options.scrollbar) {
            this.$scrollbar = generateScrollbar(this, this.options.direction);
            this.resizeScrollbar();

            $(window).on('resize.st.hammerscroll-bar' + this.guid, null, this, this.resizeScrollbar);
        }

        this.$element.on('DOMMouseScroll.st.hammerscroll mousewheel.st.hammerscroll', null, this, onMouseScroll);

        if (!this.options.eventDelegated) {
            this.hammer = new Hammer(this.$element.get(0), $.extend(true, {}, this.options.hammerOptions));

            this.hammer.get('pan').set({ direction: getHammerDirection(this) });
            this.hammer.get('swipe').set({ enable: false });
            this.hammer.get('tap').set({ enable: false });

            this.hammer.on('panstart', $.proxy(this.onDragStart, this));
            this.hammer.on('pan', $.proxy(this.onDrag, this));
            this.hammer.on('panend', $.proxy(this.onDragEnd, this));
        }

        if (null !== this.options.scrollPosition && 0 < this.options.scrollPosition) {
            this.scrollPosition(this.options.scrollPosition);
        }
    },
        old;

    /**
     * Vertical scrollbar direction.
     *
     * @type {string}
     */
    HammerScroll.DIRECTION_VERTICAL = 'vertical';
    /**
     * Horizontal scrollbar direction.
     *
     * @type {string}
     */
    HammerScroll.DIRECTION_HORIZONTAL = 'horizontal';

    /**
     * Defaults options.
     *
     * @type {object}
     */
    HammerScroll.DEFAULTS = {
        contentWrapperClass: 'hammer-scroll-content',
        maxBounce:           100,
        eventDelegated:      false,
        hammerStickyHeader:  false,
        inertiaVelocity:     0.7,
        inertiaDuration:     0.2,
        inertiaFunction:     'ease',
        scrollbar:           true,
        scrollbarInverse:    false,
        scrollbarMinSize:    14,
        contentSelector:     null,
        autoConfig:          true,
        mobileAutoConfig:    true,
        useScroll:           false,
        nativeScroll:        false,
        forceNativeScroll:   false,
        direction:           HammerScroll.DIRECTION_VERTICAL,
        scrollPosition:      null,
        hammerOptions:       {
            cssProps: {
                userSelect: 'all'
            }
        }
    };

    /**
     * On drag start action.
     *
     * @param {object} event The hammer event
     *
     * @typedef {Number} event.direction The hammer direction const
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.onDragStart = function (event) {
        this.$content.css('user-select', 'none');
        event.preventDefault();
        this.dragDirection = event.direction;
        this.$content.off('click.st.hammerscroll dragstart.st.hammerscroll drag.st.hammerscroll dragend.st.hammerscroll', 'a', preventClick);
        this.$content.on('click.st.hammerscroll dragstart.st.hammerscroll drag.st.hammerscroll dragend.st.hammerscroll', 'a', this, preventClick);
    };

    /**
     * On drag action.
     *
     * @param {object} event The hammer event
     *
     * @typedef {Number} Event.deltaX    The hammer delta X
     * @typedef {Number} Event.deltaY    The hammer delta Y
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.onDrag = function (event) {
        var position,
            allowDirection = this.isVertical ?
                    [Hammer.DIRECTION_UP, Hammer.DIRECTION_DOWN]
                : [Hammer.DIRECTION_LEFT, Hammer.DIRECTION_RIGHT],
            delta = this.isVertical ?
                    event.deltaY
                : event.deltaX;

        if (-1 === $.inArray(this.dragDirection, allowDirection)) {
            return;
        }

        if (this.options.useScroll) {
            position = limitPositionValue(this, delta, 0);
            setScrollPosition(this.$content, this.isVertical, position);
            refreshScrollbarPosition(this, false, getScrollPosition(this.$content, this.isVertical));

        } else {
            event.preventDefault();
            position = limitPositionValue(this, delta, this.options.maxBounce);

            changeTransition(this, this.$content, 'none');
            changeTranslate(this.$content, this.isVertical, -position);
            refreshScrollbarPosition(this, false, -position);
        }

        if (undefined !== this.stickyHeader) {
            this.stickyHeader.checkPosition();
        }

        trigger('scrolling', this);
    };

    /**
     * On drag end action.
     *
     * @param {object} event The hammer event
     *
     * @typedef {Number} Event.deltaX    The hammer delta X
     * @typedef {Number} Event.deltaY    The hammer delta Y
     * @typedef {Number} Event.velocityX The hammer velocity X
     * @typedef {Number} Event.velocityY The hammer velocity Y
     * @typedef {Event}  Event.srcEvent  The source event
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.onDragEnd = function (event) {
        var delta = this.isVertical ? event.deltaY : event.deltaX,
            velocity = this.isVertical ? event.velocityY : event.velocityX,
            position = limitPositionValue(this, delta, 0, true, velocity),
            animation = {},
            self = this;

        event.preventDefault();
        this.$content.css('user-select', '');

        if (this.options.useScroll) {
            animation = this.isVertical ? { scrollTop: position } : { scrollLeft: position };

            this.$content.animate(animation, this.options.inertiaDuration * 1000, function () {
                event.data = self;
                dragTransitionEnd(event);
            });

        } else {
            changeTransition(this, this.$content);
            position = -position;

            this.$content.on('transitionend msTransitionEnd oTransitionEnd', null, this, dragTransitionEnd);
            changeTranslate(this.$content, this.isVertical, position);
        }

        refreshScrollbarPosition(this, true, position);
        delete this.dragStartPosition;
        delete this.dragDirection;
    };

    /**
     * Destroy instance.
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.destroy = function () {
        $(window).off('resize.st.hammerscroll' + this.guid, this.resizeScroll);
        $(window).off('resize.st.hammerscroll-bar' + this.guid, this.resizeScrollbar);
        this.$content.off('scroll.st.hammerscroll', preventScroll);
        this.$content.off('scroll.st.hammerscroll', onScrolling);
        this.$content = unwrapContent(this);
        this.$element.off('DOMMouseScroll.st.hammerscroll mousewheel.st.hammerscroll', onMouseScroll);
        this.$element.css('touch-action', '');

        if (!this.options.eventDelegated) {
            this.hammer.destroy();
        }

        if (undefined !== this.stickyHeader) {
            this.stickyHeader.destroy();
        }

        this.$element.removeData('st.hammerscroll');

        delete this.$scrollbar;
        delete this.$content;
        delete this.isVertical;
        delete this.hammer;
        delete this.stickyHeader;
        delete this.dragStartPosition;
    };

    /**
     * Resizes the scoll content.
     * Moves the content on end if the end content is above of end
     * wrapper.
     *
     * @param {jQuery.Event|Event} [event]
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.resizeScroll = function (event) {
        var self = (undefined !== event) ? event.data : this,
            position,
            endPosition,
            maxEnd;

        if (undefined !== self.stickyHeader) {
            self.stickyHeader.checkPosition();
        }

        if (self.options.useScroll) {
            return;
        }

        position = getRealPosition(self.$content, self.isVertical);

        if (position >= 0) {
            changeTransition(self, self.$content, 'none');
            changeTranslate(self.$content, self.isVertical, 0);

            return;
        }

        endPosition = position + getRealOuterSize(self.$content, self.isVertical);
        maxEnd = self.isVertical ?
                self.$element.innerHeight()
            : self.$element.innerWidth();

        if (endPosition < maxEnd) {
            position += maxEnd - endPosition;

            changeTransition(self, self.$content, 'none');
            changeTranslate(self.$content, self.isVertical, position);
        }
    };

    /**
     * Resizes the scrollbar.
     *
     * @param {jQuery.Event|Event} [event]
     *
     * @typedef {HammerScroll} Event.data The hammer scroll instance
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.resizeScrollbar = function (event) {
        var self = (undefined !== event) ? event.data : this,
            useScroll,
            wrapperSize,
            contentSize,
            size,
            start;

        if (undefined === self.$scrollbar) {
            return;
        }

        useScroll = self.options.useScroll;
        wrapperSize = self.isVertical ?
                self.$element.innerHeight()
            : self.$element.innerWidth();
        contentSize = useScroll ?
                (self.isVertical ? self.$content.get(0).scrollHeight : self.$content.get(0).scrollWidth)
            : getRealOuterSize(self.$content, self.isVertical);
        size = Math.max(self.options.scrollbarMinSize, Math.round(wrapperSize * Math.min(wrapperSize / contentSize, 1)));
        start = useScroll ?
                getScrollPosition(self.$content, self.isVertical)
            : getRealPosition(self.$content, self.isVertical);

        if (size < wrapperSize) {
            self.$scrollbar.addClass('hammer-scroll-active');

        } else {
            self.$scrollbar.removeClass('hammer-scroll-active');
        }

        if (self.isVertical) {
            self.$scrollbar.height(size);
        } else {
            self.$scrollbar.width(size);
        }

        refreshScrollbarPosition(self, false, start);
    };

    /**
     * Scroll the content.
     *
     * @param {number} position
     *
     * @this HammerScroll
     */
    HammerScroll.prototype.scrollPosition = function (position) {
        var currentPosition;

        if (this.options.useScroll) {
            setScrollPosition(this.$content, this.isVertical, position);
            refreshScrollbarPosition(this, false, getScrollPosition(this.$content, this.isVertical));

        } else {
            currentPosition = limitPositionValue(this, -position, 0);

            changeTransition(this, this.$content, 'none');
            changeTranslate(this.$content, this.isVertical, -currentPosition);
            refreshScrollbarPosition(this, false, -currentPosition);
            delete this.dragStartPosition;
        }
    };

    /**
     * Get the scroll position.
     *
     * @return {Number}
     */
    HammerScroll.prototype.getScrollPosition = function () {
        if (this.options.useScroll) {
            return getScrollPosition(this.$content, this.isVertical);
        }

        return -getPosition(this.$content, this.isVertical);
    };

    /**
     * Get the max scroll position.
     *
     * @return {Number}
     */
    HammerScroll.prototype.getMaxScrollPosition = function () {
        if (this.options.useScroll) {
            return this.isVertical ?
                    this.$content.get(0).scrollHeight - this.$content.innerHeight()
                : this.$content.get(0).scrollWidth - this.$content.innerWidth();
        }

        return this.isVertical ?
                getRealOuterSize(this.$content, this.isVertical) - this.$element.innerHeight()
            : getRealOuterSize(this.$content, this.isVertical) - this.$element.innerWidth();
    };


    // HAMMER SCROLL PLUGIN DEFINITION
    // ===============================

    function Plugin(option, value) {
        var ret;

        this.each(function () {
            var $this   = $(this),
                data    = $this.data('st.hammerscroll'),
                options = typeof option === 'object' && option;

            if (!data && option === 'destroy') {
                return;
            }

            if (!data) {
                data = new HammerScroll(this, options);
                $this.data('st.hammerscroll', data);
            }

            if (typeof option === 'string') {
                ret = data[option](value);
            }
        });

        return undefined === ret ? this : ret;
    }

    old = $.fn.hammerScroll;

    $.fn.hammerScroll             = Plugin;
    $.fn.hammerScroll.Constructor = HammerScroll;


    // HAMMER SCROLL NO CONFLICT
    // =========================

    $.fn.hammerScroll.noConflict = function () {
        $.fn.hammerScroll = old;

        return this;
    };


    // HAMMER SCROLL DATA-API
    // ======================

    $(window).on('load', function () {
        $('[data-hammer-scroll="true"]').each(function () {
            var $this = $(this);
            Plugin.call($this, $this.data());
        });
    });

}));
  
//})(jQuery1);
