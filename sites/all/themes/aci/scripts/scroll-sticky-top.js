var scroll_it_wobble;
(function () {
  var $ = jQuery1;
  var reset_scroll;

  var doSticky = function () {

    $(function () {
      return $("[data-sticky_column-search]").stick_in_parent({
        parent: "[data-sticky_parent-search]",
        offset_top: 72
        , parent_recalc: '#mini-panel-bien_search_column_filter'
      });
    });

    if ($(window).width() > 768) {

      //  $('#content').attr('data-sticky_parent', '');

      (function ($) {
        $('#mini-panel-bien_row').attr('data-sticky_parent', '');
        $('#mini-panel-bien_column_left').attr('data-sticky_column', '');
      })(jQuery1);


      $(function () {
        return $("[data-sticky_column]").stick_in_parent({
          parent: "[data-sticky_parent]",
          offset_top: 134
        });
      });

      $(window).resize(function () {
        $(window).on("resize", (function (_this) {
          return function (e) {
            return $(document.body).trigger("sticky_kit:recalc");
          };
        })(this));
      });

    }

    reset_scroll = function () {
      var scroller;
      scroller = $("body,html");
      scroller.stop(true);
      if ($(window).scrollTop() !== 0) {
        scroller.animate({
          scrollTop: 0
        }, "fast");
      }
      return scroller;
    };

    window.scroll_it = function () {
      var max;
      max = $(document).height() - $(window).height();
      return reset_scroll().animate({
        scrollTop: max
      }, max * 3).delay(100).animate({
        scrollTop: 0
      }, max * 3);
    };

    window.scroll_it_wobble = function () {
      var max, third;
      max = $(document).height() - $(window).height();
      third = Math.floor(max / 3);
      return reset_scroll().animate({
        scrollTop: third * 2
      }, max * 3).delay(100).animate({
        scrollTop: third
      }, max * 3).delay(100).animate({
        scrollTop: max
      }, max * 3).delay(100).animate({
        scrollTop: 0
      }, max * 3);
    };

  };

  $(window).resize(function () {
    doSticky();
  });

  $(window).load(function () {
    doSticky();
  });

}).call(this);
