(function ($) {
    'use strict';

    // font page, full height for section-1's picture
    var doHeight = function () {
        if ($(window).width() > 580) {
            var windowHeight = $(window).height();
            $('body.front #section-1').height(windowHeight);
        }
    };
    doHeight();
    $(window).resize(function () {
        doHeight();
    });


    /* message */
    if ($('#messages')[0] !== undefined) {
        $('#messages').on('click', function () {
            $('#messages').remove();
        });
    }

    /*
     * fontawesome not applied .. 
     */
    // sidebar
    var menuItems = $('#block-system-main-menu ul.menu > li a, .pane-system-main-menu ul.menu > li a, .pane-menu-menu-submenu-infos-utilities ul.menu > li a');
    $.each(menuItems, function (i, e, _e) {
        var a = $(e),
                classValue = a.attr('class'),
                htmlValue = a.html();
        a.html('<i class="' + a.attr('class') + '"></i>' + a.html());
        a.removeClass(a.attr('class'));
    });

    // services links fiche bien
    $('.pane-service-links-service-links .service-links-print').html('<i class="fa fa-print"></i><span class="element-invisible">Print</span>');
    $('.pane-service-links-service-links .service-links-printmail').html('<i class="fa fa-share"></i><span class="element-invisible">Mail</span>');
    $('.pane-service-links-service-links .service-links-facebook').html('<i class="fa fa-share-alt"></i><span class="element-invisible">Share</span>');
    $('.pane-service-links-service-links .service-links-printpdf').html('<i class="fa fa-file-pdf-o"></i><span class="element-invisible">PDF</span>');

    // icon favoris dans fiche bien
    var flagIcon = $('#mini-panel-bien_link_flag_favoris a.flag.flag-action.flag-link-normal');
    flagIcon.html('<i class="fa fa-thumbs-o-up"></i>' + flagIcon.html());

    // icon switch open/close du sidebar
    var toggleClass = 'a#mini-panel-bien_column_left-toggle',
            i = ' #sidebar-menu-icon',
            open = 'fa-bars',
            close = 'fa-times';

    // remove sidebar in overlayed page
    $('.overlay-wrapper .sidebar-trigger.sidebar-togglable').remove();

    if ($(toggleClass) === Object) {
        $(toggleClass).on('click', function (e) {
            var target = $(toggleClass + i);
            if (target.hasClass(open)) {
                target.removeClass(open);
                target.addClass(close);
            } else {
                target.removeClass(close);
                target.addClass(open);
            }
        });
    }

    $('#mini-panel-bien_description_link a').on('click', function (e) {
        var posY = window.scrollY;

        jQuery(document).bind('drupalOverlayBeforeLoad', function (e) {
            var scroller = $("body,html");
            scroller.stop(true);
            scroller.animate({
                scrollTop: posY
            }, "fast");
            return false;
        });

        jQuery(document).bind('drupalOverlayClose', function (e) {
            var scroller = $("body,html");
            scroller.animate({
                scrollTop: posY
            }, "fast");
            return true;
        });
    });

    (function ($) {
        $('#mini-panel-estate_search_row').attr('data-sticky_parent-search', '');
        $('#bien-search-rigth').attr('data-sticky_column-search', '');
    })(jQuery1);


    $('.facetapi-active').parent().addClass('facetapi-active-wrapper');
    var span = $('.facetapi-active span.element-invisible');
    $('a.facetapi-active').html('<i class="fa fa-times"></i>');

    $('.sidebar-toggle-rigth').wrap('<div class="sidebar-toggle-rigth-wrapper"></div>');

//    console.log('Flags');
//    var langImg  = $('.lang-noflag li').find('img');
//    $.each(langImg, function(i, el) {
//        console.log(el);
//    });

    $.each($('.pane-locale-language-content, #block-locale-language-content').find('li a'), function (i, li) {
        var img = $(li).find('img');
        $(li).html('').html(img);
    });

    // parallax
    $('#section-1').parallax({imageSrc: '/sites/all/themes/aci/css/images/home-photo-2_02.jpg'});
    
    var h2s = $('h2');
    $.each(h2s, function(i, h2) {
        if($(h2).html() === "")
            h2.remove();
    });
    
})(jQuery1);
